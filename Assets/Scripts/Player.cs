﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private int _blinkCount;
    [SerializeField]
    private int _lives;
    [SerializeField]
    private float _maxY;
    [SerializeField]
    private float _minY;
    [SerializeField]
    private float _speedMovement;
    [SerializeField]
    private float _yMovement;

    private Vector2 _targetPosition;

    private void Start()
    {
        _targetPosition = transform.position;
    }

    private void Update()
    {
        HandleInput();
        UpdatePosition();
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && _targetPosition.y < _maxY)
        {
            _targetPosition = new Vector2(_targetPosition.x, _targetPosition.y + _yMovement);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && _targetPosition.y > _minY)
        {
            _targetPosition = new Vector2(_targetPosition.x, _targetPosition.y - _yMovement);
        }
    }

    private void UpdatePosition()
    {
        if (transform.position.y != _targetPosition.y)
        {
            transform.position = Vector2.MoveTowards(transform.position, _targetPosition, _speedMovement * Time.deltaTime);
        }
    }

    public void TakeDamage(int damage)
    {
        _lives -= damage;
        StartCoroutine(Blink());
        if (_lives <= 0)
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator Blink()
    {
        for (int i = 0; i < _blinkCount; i++)
        {
            Renderer renderer = gameObject.GetComponent<Renderer>();
            renderer.enabled = false;
            yield return new WaitForSeconds(0.1f);
            renderer.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
