﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private int _damage;
    [SerializeField]
    private float _speed;

    private void Update()
    {
        transform.Translate(Vector2.left * _speed * Time.deltaTime);

        Vector3 position = Camera.main.WorldToScreenPoint(transform.position);
        Renderer renderer = GetComponent<Renderer>();
        if (position.x < 0f && !renderer.isVisible)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player player = collision.GetComponent<Player>();
            player.TakeDamage(_damage);
            Destroy(gameObject);
        }
    }
}
