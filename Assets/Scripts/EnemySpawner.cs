﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject _enemy;
    [SerializeField]
    private float _initialTimeBetweenSpawn;
    [SerializeField]
    private float _maxY;
    [SerializeField]
    private float _minTimeBetweenSpawn;
    [SerializeField]
    private float _minY;
    [SerializeField]
    private float _timeDecrease;

    private float _timeBetweenSpawn;

    private void Start()
    {
        _timeBetweenSpawn = _initialTimeBetweenSpawn;
    }

    private void Update()
    {
        if (_timeBetweenSpawn <= 0)
        {
            int y1 = Random.Range(-1, 2);
            int y2 = Random.Range(-1, 2);
            Instantiate(_enemy, new Vector3(transform.position.x, y1 * 2), Quaternion.identity);
            if (y1 != y2)
            {
                Instantiate(_enemy, new Vector3(transform.position.x, y2 * 2), Quaternion.identity);
            }
            _timeBetweenSpawn = _initialTimeBetweenSpawn;
            if (_initialTimeBetweenSpawn > _minTimeBetweenSpawn)
            {
                _initialTimeBetweenSpawn -= _timeDecrease;
            }
            if (_initialTimeBetweenSpawn < _minTimeBetweenSpawn)
            {
                _initialTimeBetweenSpawn = _minTimeBetweenSpawn;
            }
        }
        else
        {
            _timeBetweenSpawn -= Time.deltaTime;
        }
    }
}
